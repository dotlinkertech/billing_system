@include('layouts.app')

<body style="background-color: aliceblue;">
    <div class="container">
        <div class="row justify-content-center">
            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                </symbol>
            </svg>
            @if(session('csadded'))
            <div class="alert alert-success alert-dismissible d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                    <use xlink:href="#check-circle-fill" />
                </svg>
                <div>
                    {{session('csadded')}}
                </div>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            @if(session('csedited'))
            <div class="alert alert-success alert-dismissible d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                    <use xlink:href="#check-circle-fill" />
                </svg>
                <div>
                    {{session('csedited')}}
                </div>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            @if(session('csdeleted'))
            <div class="alert alert-success alert-dismissible d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                    <use xlink:href="#check-circle-fill" />
                </svg>
                <div>
                    {{session('csdeleted')}}
                </div>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            <!-- <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div> -->
            <div class="mb-4">
                <button class="btn btn-info" data-bs-toggle="modal" data-bs-target="#addcustomer">+ Add Customer</button>
            </div>
            <div class="modal fade" id="addcustomer" tabindex="-1" aria-labelledby="addcustomerLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addcustomerLabel">Add new customer details</h5>
                            <p class="btn btn-close" data-bs-dismiss="modal" aria-label="Close"></p>
                        </div>
                        <form class="modal-body" id="add_cs_detail">
                            @csrf
                            <div class="mb-3">
                                <label for="server_ip" class="form-label">Server Ip</label>
                                <input type="text" class="form-control" id="server_ip" name="server_ip" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="cspl" class="form-label">Customer Social Profile Link</label>
                                <input type="text" class="form-control" id="cspl" name="customer_profile_link" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="buy_date" class="form-label">Buy Date</label>
                                <input type="date" class="form-control" id="buy_date" name="buy_date" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="expire_date" class="form-label">Expire Date</label>
                                <input type="date" class="form-control" id="expire_date" name="expire_date" aria-describedby="emailHelp">
                            </div>
                            <button class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editcustomer" tabindex="-1" aria-labelledby="editcustomerLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editcustomerLabel">Edit customer details</h5>
                            <p class="btn btn-close" data-bs-dismiss="modal" aria-label="Close"></p>
                        </div>
                        <form class="modal-body" id="edit_cs_detail">
                            @csrf
                            <div class="mb-3">
                                <label for="ide" class="form-label">Id</label>
                                <input type="text" class="form-control" id="ide" name="id" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="server_ipe" class="form-label">Server Ip</label>
                                <input type="text" class="form-control" id="server_ipe" name="server_ip">
                            </div>
                            <div class="mb-3">
                                <label for="csple" class="form-label">Customer Social Profile Link</label>
                                <input type="text" class="form-control" id="csple" name="customer_profile_link">
                            </div>
                            <div class="mb-3">
                                <label for="buy_datee" class="form-label">Buy Date</label>
                                <input type="date" class="form-control" id="buy_datee" name="buy_date">
                            </div>
                            <div class="mb-3">
                                <label for="expire_datee" class="form-label">Expire Date</label>
                                <input type="date" class="form-control" id="expire_datee" name="expire_date">
                            </div>
                            <button class="btn btn-primary">Edit</button>
                        </form>
                    </div>
                </div>
            </div>
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Server Ip</th>
                        <th>Customer Social Profile Link</th>
                        <th>Buy Date</th>
                        <th>Expire Date</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $d)
                    <tr>
                        <td>{{$d->id}}</td>
                        <td>{{$d->server_ip}}</td>
                        <td><a href="{{$d->customer_profile_link}}" class="btn btn-info" target="_blank">Click to see</a></td>
                        <td>{{$d->buy_date}}</td>
                        <td>{{$d->expire_date}}</td>
                        @if($d->expire_date < date('Y-m-d')) <td><button class="btn btn-danger">EXPIRED</button></td>
                            @else
                            <td><button class="btn btn-success">ACTIVE</button></td>
                            @endif
                            <td>{{$d->created_at}}</td>
                            <td>{{$d->updated_at}}</td>
                            <td>
                                <button class="btn btn-warning" onclick="editCustomer('{{$d->id}}', '{{$d->server_ip}}', '{{$d->customer_profile_link}}', '{{$d->buy_date}}', '{{$d->expire_date}}')" data-bs-toggle="modal" data-bs-target="#editcustomer">Edit</button>
                            </td>
                            <td>
                                <button class="btn btn-danger" onclick="delCustomer('{{$d->id}}')">Delete</button>
                            </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Server Ip</th>
                        <th>Customer Social Profile Link</th>
                        <th>Buy Date</th>
                        <th>Expire Date</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</body>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollX": true,
            "columnDefs": [{
                "className": "dt-center",
                "targets": "_all"
            }]
        });
    });
</script>
<script>
    $('#add_cs_detail').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "{{asset('add/customerdetails')}}",
            data: $('#add_cs_detail').serialize(),
            success: function(res) {
                if (res == 'success') {
                    $('#addcustomer').modal('hide');
                    location.reload();
                }
            },
            error: function(res) {
                console.log(res);
            }
        })
    })
</script>
<script>
    function editCustomer(a, b, c, d, e) {
        $('[id=ide]').val(a);
        $('[id=server_ipe]').val(b);
        $('[id=csple]').val(c);
        $('[id=buy_datee]').val(d);
        $('[id=expire_datee]').val(e);
    }
    $('#edit_cs_detail').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "{{asset('edit/customerdetails')}}",
            data: $('#edit_cs_detail').serialize(),
            success: function(res) {
                if (res == 'success') {
                    $('#editcustomer').modal('hide');
                    location.reload();
                }
            },
            error: function(res) {
                console.log(res);
            }
        });
    })

    function delCustomer(x) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCloseButton: true,
            confirmButtonColor: '#3085d6',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    url: "{{asset('delete/customerdetails')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": x
                    },
                    success: function(res) {
                        if (res == 'success') {
                            location.reload();
                        }
                    },
                    error: function(res) {
                        console.log(res);
                    }
                })
            }
        })
    }
</script>