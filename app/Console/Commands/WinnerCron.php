<?php

namespace App\Console\Commands;

use App\Models\Customer;
use Illuminate\Console\Command;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Exception as GlobalException;

class WinnerCron extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'winner:cron';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set("Asia/Kolkata");
        $currdate = date('Y-m-d');
        $datas = Customer::where('expire_date', $currdate)->get();

        $profiles = "";
        foreach ($datas as $key) {
            $profiles .= "<tr><td>" . $key->server_ip . "</td><td><a href='$key->customer_profile_link'>" . $key->customer_profile_link . "<a></td> <td>" . $key->buy_date . "</td><td>" . $key->expire_date . "</td><tr>";
        }

        require base_path("vendor/autoload.php");


        try {

            $mail = new PHPMailer(true);

            $mail->isSMTP();                        // Set mailer to use SMTP
            $mail->Host       = 'smtp.zoho.in';    // Specify main SMTP server
            $mail->SMTPAuth   = true;               // Enable SMTP authentication
            $mail->Username   = 'info@insuranceaspect.in';     // SMTP username
            $mail->Password   = 'Kolkata@123';         // SMTP password
            $mail->SMTPSecure = 'TLS';              // Enable TLS encryption, 'ssl' also accepted
            $mail->Port       = 587;

            $mail->setFrom('info@insuranceaspect.in', 'Billing Reminder');           // Set sender of the mail
            $mail->addAddress('shreyandey5@gmail.com');           // Add a recipient

            $mail->isHTML(true);
            $mail->Subject = 'End Date reminder!';
            $mail->Body = '<!DOCTYPE html>
                            <html lang="en">

                            <head>
                                <meta charset="UTF-8">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                <title>Billing</title>
                                <style>
                                    th {
                                        padding: 10px;
                                    }
                                    td{
                                        padding: 10px;
                                    }
                                </style>
                            </head>

                            <body style="background: aliceblue; font-family:serif;">
                                <div style="margin:0px auto; height: 100vh; display: flex; justify-content: center; align-items: center;">
                                    <div>
                                        <div style="text-align: center;">
                                            <h2>The Lists of the profiles that are ending today</h2>
                                        </div>
                                        <table border="1px solid blue" style="border-collapse: collapse; font-size: 20px; width: 100%;">
                                            <thead style="background: #0dcaf0;">
                                                <tr>
                                                    <th scope="col">Server IP</th>
                                                    <th scope="col">Social Profile Link</th>
                                                    <th scope="col">Buy Date</th>
                                                    <th scope="col">Expire Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            ' .
                $profiles
                . '
                                            </tbody>
                                            <tfoot style="background: #0dcaf0;">
                                                <tr>
                                                    <th scope="col">Server IP</th>
                                                    <th scope="col">Social Profile Link</th>
                                                    <th scope="col">Buy Date</th>
                                                    <th scope="col">Expire Date</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>';
            $mail->send();
        } catch (GlobalException $e) {
            echo $e;
        }
    }
}
