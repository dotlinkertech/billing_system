<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data =  Customer::get();
        return view('home', ['data' => $data]);
    }
    public function Add(Request $req)
    {
        $data = $req->input();
        $acs = [
            'server_ip' => $data['server_ip'],
            'customer_profile_link' => $data['customer_profile_link'],
            'buy_date' => $data['buy_date'],
            'expire_date' => $data['expire_date'],
            'created_at' => date('Y/m/d H:i')
        ];
        $add = Customer::insert($acs);
        if ($add) {
            $req->session()->flash('csadded', 'Customer details added successfully!');
            return 'success';
        }
    }
    public function Edit(Request $req)
    {
        $data = $req->input();
        $ecs = [
            'server_ip' => $data['server_ip'],
            'customer_profile_link' => $data['customer_profile_link'],
            'buy_date' => $data['buy_date'],
            'expire_date' => $data['expire_date'],
            'updated_at' => date('Y/m/d H:i')
        ];
        $edit = Customer::where('id', $data['id'])->update($ecs);
        if ($edit) {
            $req->session()->flash('csedited', 'Customer details edited successfully!');
            return 'success';
        }
    }
    public function Delete(Request $req)
    {
        $del = Customer::where('id', $req->input()['id'])->delete();
        if ($del) {
            $req->session()->flash('csdeleted', 'Customer details deleted successfully!');
            return 'success';
        }
    }
}
