<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ReminderEndDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $req) {
    return redirect('/login');
});

Auth::routes();
Auth::routes(['verify' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/add/customerdetails', [HomeController::class, 'Add']);
Route::post('/edit/customerdetails', [HomeController::class, 'Edit']);
Route::post('/delete/customerdetails', [HomeController::class, 'Delete']);
Route::get('sendMail', [ReminderEndDate::class, 'index']);
